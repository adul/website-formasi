<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mailing extends CI_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model('schedule_model','Schedule');
		$this->load->model('email_model','Mail');
		$this->load->model('pelanggan_model','Pelanggan');
	}
	
	function index(){
		$this->schedule_progress();
	}

	function get_next_task() {
		
		
		$ret = $this->Schedule->get_next_task();
		
		$list	= get_object_vars($ret);
		echo json_encode($list)	;

	}

	function update_schedule_status($id,$status) {
		$data = array();
		$data['id'] = $id;
		$data['status'] = $status;
		$data['sent_date'] = date("Y-m-d H:i:s");
		$this->Schedule->save($data);
	}

	function new_mail_schedule() {
		$this->load->helper(array('form', 'url'));

		$tpl['arr_email'] = $this->Mail->get_list();

		$this->load->view('back/email_blast/new_mail_schedule',array('tpl' => $tpl));
		
	}

	function proses_file() {

		$config['upload_path'] = './uploads/data_pelanggan/';
		$config['allowed_types'] = 'csv';
		$config['max_size']	= '1024';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);
		$tpl = array();
		if ( ! $this->upload->do_upload())
		{
			$tpl['error'] = array('error' => $this->upload->display_errors());
			var_dump($_FILES);
			$this->load->view('back/email_blast/new_mail_schedule',array('tpl' => $tpl));
		}
		else
		{
			$email_id = $this->input->post('email_id');
			$result = $this->Pelanggan->save_bulk($this->upload->data());
			$tpl['data'] = array();
			foreach ($result as $key => $value) {
				$data = $value;
				$data['email_id'] = $email_id; 
				$data['input_date'] = date("Y-m-d H:i:s");
				$id = $this->Schedule->save($data);

				$tpl['data'][] = $this->Schedule->get_by_id($id);
			}


			$this->load->view('back/email_blast/upload_succeed',array('tpl' => $tpl));
		}
	}

	function schedule_progress() {
		
		$tpl =  array();
		$tpl['data'] = $this->Schedule->get_all();
		$this->load->view('back/email_blast/schedule_progress',array('tpl' => $tpl));
	}

	function email_list() {
		$tpl = array();
		$tpl['data']	= $this->Mail->get_list();
		$this->load->view('back/email_blast/email_list',array('tpl' => $tpl) );
	}

	function email_create() {
		if ($this->input->post('email_create')) {
			$data = $this->input->post();
			$this->Mail->save($data);
			$this->email_list();
		} else {
			$this->load->view('back/email_blast/email_create' );

		}
	}

	function email_update($id = 0) {
		if ($this->input->post('email_update')) {
			$data = $this->input->post();
			$this->Mail->save($data);
			$this->email_list();
		} else {
			$tpl = array();
			$tpl['data']	= $this->Mail->get_by_id($id);
			$this->load->view('back/email_blast/email_update',array('tpl' => $tpl) );

		}
	}

	function email_view($id) {
		$tpl = array();
		$tpl['data']	= $this->Mail->get_by_id($id);
		$this->load->view('back/email_blast/email_view',array('tpl' => $tpl) );
	}


}