<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 function __construct()
	{
		parent::__construct();
		$this->load->model('update_model','Update');
		$this->load->helper('general_helper');
	}
	public function update_taxes()
	{
		
		$tpl['indBulan'] = indBulan();
		$this->load->view('front/header');
		$this->load->view('front/download/update_taxes/update_taxes',array('tpl' => $tpl ));
		$this->load->view('front/footer');

	}
	
	public function show_detail($bln,$thn)
	{
		$tpl['data'] =	$this->Update->get_by_bulan_tahun($bln,$thn);
		$tpl['indBulan'] = indBulan();
		$content = $this->load->view('front/download/update_taxes/update_taxes_content',array('tpl' => $tpl));
		return $content;
		exit;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */