<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fortax extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('front/header');
		$this->load->view('front/product/fortax/presentasi');
		$this->load->view('front/footer');
	}

	public function demo()
	{
		$this->load->view('front/header');
		$this->load->view('front/product/fortax/fortax');
		$this->load->view('front/footer');	
	}

	public function brosur() {
		$this->load->view('front/header');
		$this->load->view('front/product/fortax/brosur');
		$this->load->view('front/footer');

	}

	public function order()
	{
		$this->load->view('front/header');
		$this->load->view('front/product/fortax/order');
		$this->load->view('front/footer');	
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */