<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'libraries/swiftmailer/swift_required.php');

class Email
{
	var $from = array();
	var $recipient = array();
	var $subject = '';
	var $body = '';

	var $err_msg;

	public function send() {
        if (count($this->from) < 1 ) {

        	$this->err_msg = 'from email must be given';
        	return 0;
        }
       

       $transport = Swift_SmtpTransport::newInstance('localhost', 25);

        $mailer = Swift_Mailer::newInstance($transport);

        $message = Swift_Message::newInstance($this->subject)
          ->setFrom(array('reseller@pustaka-lebah.com' => 'PUSTAKA LEBAH'))
          ->setTo($this->recipient)
          ->setBody($this->body,'text/html');

        return $mailer->send($message);
        
    }


   

}





?>