<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Order_Item_model extends CI_Model_Mod {

    function __construct()
    {
        parent::__construct();

        $this->table = 'order_items';
        $this->scheme = array(
            'id',
            'order_id',
            'paket_id',
            'quantity', 
            'amount'
        );
    }
    
    

}

?>