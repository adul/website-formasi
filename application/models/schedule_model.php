<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedule_Model extends CI_Model {

	var $table = 'tb_email_schedule';

    var $scheme = array(
            'id',
            'pelanggan_id',
            'email_id',
            'input_date',
            'sent_date',
            'status'
        );

    function getScheme($data) {
		$arr = array();
		foreach ($data as $key => $value) {
            if (in_array($key, $this->scheme)) {
            	$arr[$key] = $value;
            }
        }
        return $arr;
	}

	function __construct()
	{
		parent::__construct();
	}

	 function get_all() {
        $this->db->select('s.*,e.judul,e.isi,p.sex,p.nama,p.email');
        $this->db->from('tb_email_schedule s' );
        $this->db->join('tb_email e' ,'e.id = s.email_id');
        $this->db->join('tb_pelanggan p' ,'p.id = s.pelanggan_id');
        $this->db->order_by('status','asc');
        $this->db->order_by('id','desc');

        
               
        $query = $this->db->get();
        $query = $query->result();
        if (count($query) > 0 ) {
            return $query;
        } else {
            return false;
        }

    }

    function get_by_id($id) {
        $this->db->select('s.*,e.judul,e.isi,p.sex,p.nama,p.email');
        $this->db->from('tb_email_schedule s' );
        $this->db->join('tb_email e' ,'e.id = s.email_id');
        $this->db->join('tb_pelanggan p' ,'p.id = s.pelanggan_id');
        $this->db->where('s.id',$id);
       
        
        $query = $this->db->get();
        $query = $query->result();
        if (count($query) > 0 ) {
            return $query[0];
        } else {
            return false;
        }

    }

    function get_next_task()
    {
        $this->db->select('id');
        $this->db->from($this->table );
        $this->db->where('status',0);
        $this->db->order_by('id','desc');

        $this->db->limit(1);
        $query = $this->db->get();
        $query = $query->result();

            
        if (count($query) > 0 ) {
            $id = $query[0]->id;
            return   $this->get_by_id($id);

        } else {
            return 0;
        }
        
    }

    function save($data) {
        $arr = $this->getScheme($data);
        if (isset($arr['id'])) {
            $this->db->update($this->table, $arr, array('id' => $arr['id']));
            return $arr['id'];
        } else {
            $this->db->insert($this->table,$arr);
            return $this->db->insert_id();
        }
    }


}
