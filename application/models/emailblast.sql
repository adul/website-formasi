/*
SQLyog Ultimate - MySQL GUI v8.2 
MySQL - 5.1.72-cll : Database - k4349636_formasi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`k4349636_formasi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `k4349636_formasi`;

/*Table structure for table `tb_email` */

DROP TABLE IF EXISTS `tb_email`;

CREATE TABLE `tb_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `isi` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tb_email` */

insert  into `tb_email`(`id`,`judul`,`isi`) values (1,'Penawaran Fortax','<div style=\"width: 100%;height: 30px;background-color: blue;padding-top:10px\">\n	<a href=\"http://formasi.com/index.php/fortax/brosur\">\n		<div style=\"width: 100%;;height: 50px;text-align:center\">\n			<span style=\"color:white\">	Jika kesulitan melihat seluruh isi pesan ini, klik  di sini</span>\n		</div>\n	</a>\n	\n</div >\n\n<div style=\"width: 100%;text-align:center\" >\n	<img src=\"http://pustaka-lebah.com/beeschool/brosur1.png\">\n</div>\n\n<br />\n<div style=\"width: 100%;;height: 50px\">\n	<a href=\"http://www.formasi.com/fortax\">\n		<div style=\"width: 100%;;height: 50px;text-align:center\">\n			<span style=\"font-size:24px\">	Klik di sini untuk informasi lebih lanjut</span>\n		</div>\n	</a>\n	\n</div >'),(2,'Penawaran Beeschool','ini adalah penawaran beeschool');

/*Table structure for table `tb_email_schedule` */

DROP TABLE IF EXISTS `tb_email_schedule`;

CREATE TABLE `tb_email_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pelanggan_id` int(11) NOT NULL,
  `email_id` int(11) NOT NULL,
  `input_date` datetime DEFAULT NULL,
  `sent_date` datetime DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `tb_email_schedule` */

insert  into `tb_email_schedule`(`id`,`pelanggan_id`,`email_id`,`input_date`,`sent_date`,`status`) values (1,1,1,NULL,'2014-01-18 12:15:27',1),(2,3,2,'2014-01-18 11:48:48','2014-01-18 12:16:03',1),(3,4,2,'2014-01-18 11:48:48','2014-01-18 12:15:54',1),(4,5,2,'2014-01-18 11:48:48','2014-01-18 12:15:45',1),(5,6,1,'2014-01-18 12:21:13','2014-01-18 12:21:33',1),(6,7,1,'2014-01-18 12:21:13','2014-01-18 12:21:28',1),(7,8,1,'2014-01-18 12:21:13','2014-01-18 12:21:22',1),(8,9,1,'2014-01-18 12:26:32','2014-01-18 12:26:52',1),(9,10,1,'2014-01-18 12:26:32','2014-01-18 12:26:46',1),(10,11,1,'2014-01-18 12:26:32','2014-01-18 12:26:40',1),(11,12,1,'2014-01-18 12:47:56','2014-01-18 12:48:06',1),(12,13,1,'2014-01-18 12:52:13','2014-01-18 12:52:17',1),(13,14,1,'2014-01-18 12:55:51','2014-01-18 12:56:24',1),(14,15,1,'2014-01-18 12:55:51','2014-01-18 12:56:18',1),(15,16,1,'2014-01-18 12:55:51','2014-01-18 12:56:12',1),(16,17,1,'2014-01-18 12:55:51','2014-01-18 12:56:06',1),(17,18,1,'2014-01-18 12:55:51','2014-01-18 12:56:00',1),(18,19,1,'2014-01-20 03:15:55','2014-01-20 03:20:47',1),(19,20,1,'2014-01-20 03:15:55','2014-01-20 03:20:42',1),(20,21,1,'2014-01-20 03:15:55','2014-01-20 03:20:36',1);

/*Table structure for table `tb_pelanggan` */

DROP TABLE IF EXISTS `tb_pelanggan`;

CREATE TABLE `tb_pelanggan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sex` varchar(255) DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `tb_pelanggan` */

insert  into `tb_pelanggan`(`id`,`sex`,`nama`,`email`) values (1,NULL,'awin','awin.poenye@gmail.com'),(2,NULL,'adul','smile.adul@gmail.com'),(3,NULL,'awin','awin.poenye@gmail.com'),(4,NULL,'djoko','saya.juga@gmail.com'),(5,NULL,'gunadi','gunadi@yahoo.com'),(6,NULL,'awin','awin.poenye@gmail.com'),(7,NULL,'djoko','saya.juga@gmail.com'),(8,NULL,'gunadi','gunadi@yahoo.com'),(9,NULL,'awin','awin.poenye@gmail.com'),(10,NULL,'djoko','saya.juga@gmail.com'),(11,NULL,'gunadi','gunadi@yahoo.com'),(12,NULL,'gunadi','hhastowo@yahoo.com'),(13,NULL,'awin','awin.poenye@gmail.com'),(14,NULL,'gunadi','hhastowo@yahoo.com'),(15,NULL,'joko','djokopurnomo.pustakalebah@gmail.com'),(16,NULL,'aswin','awin.poenye@gmail.com'),(17,NULL,'gunadi','gunadi@pustaka-lebah.com'),(18,NULL,'awin','syirojul_1981@yahoo.com'),(19,NULL,'awin','awin.poenye@gmail.com'),(20,NULL,'adul','smile.adul@gmail.com'),(21,NULL,'gunadi','hhastowo@yahoo.com');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
