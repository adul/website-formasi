<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'models/ci_model_mod.php');

class Paket_model extends CI_Model_Mod {

    function __construct()
    {
        parent::__construct();

        $this->table = 'paket';
        $this->scheme = array(
            'id',
            'product_id',
            'name',
            'short_description', 
            'description',
            'quantity',
            'status',
            'price'
        );
    }

    function get_by_id($id) {
        $opt['select'] = 'p.*,d.quantity disc_qty, d.multiple, d.discount';
        $opt['from'] = 'paket p';
        $opt['join']['discount d'] = array("d.paket_id = p.id ","left");
        $opt['where']['p.id'] = $id;
        $result = $this->get_list($opt);
        if (count($result) > 0) {
            return $result[0];
        } else {
            return false;
        }

    }


}

?>