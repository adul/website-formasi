<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pelanggan_Model extends CI_Model {

	var $table = 'tb_pelanggan';

    var $scheme = array(
            'id',
            'sex',
            'nama',
            'email'
        );

    function getScheme($data) {
        $arr = array();
        foreach ($data as $key => $value) {
            if (in_array($key, $this->scheme)) {
                $arr[$key] = $value;
            }
        }
        return $arr;
    }


	function __construct()
	{
		parent::__construct();
	}

        
        function object_to_array($object)
         {
          if (is_object($object))
          {
           // Gets the properties of the given object with get_object_vars function
           $object = get_object_vars($object);
          }
         
           return (is_array($object)) ? array_map(__FUNCTION__, $object) : $object;
         }	

    function get_by_id($id) {
        $this->db->select('*');
        $this->db->from($this->table);
        
        $this->db->where("id", $id);
        
        $query = $this->db->get();
        $query = $query->result();
        if (count($query) > 0 ) {
            $query = $query[0];
            return  object_to_array($query );
        } else {
            return false;
        }
    }

    function save($data) {
        $arr = $this->getScheme($data);
        if (isset($arr['id'])) {
            $this->db->update($this->table, $arr, array('id' => $arr['id']));
            return $arr['id'];
        } else {
            $this->db->insert($this->table,$arr);
            return $this->db->insert_id();
        }
    }

    function delete($id) {
        
        $q = 'delete from ' . $this->table . ' where id=' . $id;
        return $this->db->query($q);

    }

     function save_bulk($file_data){
        $fl = fopen($file_data['full_path'], 'r');
        $nomor = 0;
        $tmp = array();
        
        while( ($row = fgetcsv($fl, 0, ',')) != false ) { 
            if ($nomor > 0) {
                
                $data = array();
                $data['nama'] = $row[0];
                $data['email'] = $row[1];
                if(!empty($row[0])){
                    $id = $this->save($data);
                    $data['pelanggan_id'] = $id;
                    $tmp[] = $data;
                }
            }
            $nomor++;
        }
        fclose($fl);
        return $tmp;
    }

}
