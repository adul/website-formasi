<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Update_Model extends CI_Model {

	var $table = 'tb_update';

    var $scheme = array(
            'id',
            'edisi',
            'bulan',
            'tahun',
			'ukuran',
			'ditail'
        );

    function getScheme($data) {
        $arr = array();
        foreach ($data as $key => $value) {
            if (in_array($key, $this->scheme)) {
                $arr[$key] = $value;
            }
        }
        return $arr;
    }


	function __construct()
	{
		parent::__construct();
	}

    function get_by_id($id) {
        $this->db->select('*');
        $this->db->from($this->table);
        
        $this->db->where("id", $id);
        
        $query = $this->db->get();
        $query = $query->result();
        if (count($query) > 0 ) {
            return $query[0];
        } else {
            return false;
        }
    }
	
	function get_by_bulan_tahun($bln,$thn) {
        $this->db->select('*');
        $this->db->from($this->table);
        
        $this->db->where("bulan", $bln);
        $this->db->where("tahun", $thn);
		
        $query = $this->db->get();
        $query = $query->result();
        if (count($query) > 0 ) {
            return $query;
        } else {
            return false;
        }
    }

    function save($data) {
        $arr = $this->getScheme($data);
        if (isset($arr['id'])) {
            $this->db->update($this->table, $arr, array('id' => $arr['id']));
            return $arr['id'];
        } else {
            $this->db->insert($this->table,$arr);
            return $this->db->insert_id();
        }
    }

    function delete($id) {
        
        $q = 'delete from ' . $this->table . ' where id=' . $id;
        return $this->db->query($q);

    }

    

}
