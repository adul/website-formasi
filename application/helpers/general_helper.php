<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	

	function isLogged($forceLogin = false) {
		$CI = & get_instance();
		
		$CI->load->library('session'); 
		$user = $CI->session->userdata('user');
	    if ($user) {
	    	return $user;
	    } else {
	    	if ($forceLogin) {
				redirect(base_url('member/noAccess'));
			}
			return false;
	    }
	}
	function provinceTeks($id) {
		switch ($id) {
			case 1 : $teks = 'Nanggro Aceh Darussalam'; break;
			case 2 : $teks = 'Sumatera Utara'; break;
			case 3 : $teks = 'Sumatera Barat'; break;
			case 4 : $teks = 'Riau'; break;
			case 5 : $teks = 'Kepulauan Riau'; break;
			case 6 : $teks = 'Jambi'; break;
			case 7 : $teks = 'Sumatera Selatan'; break;
			case 8 : $teks = 'Kep. Bangka Belitung'; break;
			case 9 : $teks = 'Bengkulu'; break;
			case 10 : $teks = 'Lampung'; break;
			case 11 : $teks = 'DKI Jakarta'; break;
			case 12 : $teks = 'Jawa Barat'; break;
			case 13 : $teks = 'Banten'; break;
			case 14 : $teks = 'Jawa Tengah'; break;
			case 15 : $teks = 'DI Yogyakarta'; break;
			case 16 : $teks = 'Jawa Timur'; break;
			case 17 : $teks = 'Bali'; break;
			case 18 : $teks = 'Nusa Tenggara Barat'; break;
			case 19 : $teks = 'Nusa Tenggara Timur'; break;
			case 20 : $teks = 'Kalimantan Barat'; break;
			case 21 : $teks = 'Kalimantan Tengah'; break;
			case 22 : $teks = 'Kalimantan Selatan'; break;
			case 23 : $teks = 'Kalimantan Timur'; break;
			case 24 : $teks = 'Kalimantan Utara'; break;
			case 25 : $teks = 'Sulawesi Utara'; break;
			case 26 : $teks = 'Sulawesi Barat'; break;
			case 27 : $teks = 'Sulawesi Tengah'; break;
			case 28 : $teks = 'Sulawesi Tenggara'; break;
			case 29 : $teks = 'Sulawesi Selatan'; break;
			case 30 : $teks = 'Gorontalo'; break;
			case 31 : $teks = 'Maluku'; break;
			case 32 : $teks = 'Maluku Utara'; break;
			case 33 : $teks = 'Papua'; break;
			case 34 : $teks = 'Papua Barat'; break;

			default:
				$teks = "";
				break;
		}
		return $teks;
	}

	function getCities($province){
		switch ($province) {
			case 1:
				return array('Aceh Barat', 'Aceh Barat Daya', 'Aceh Besar', 'Aceh Jaya', 'Aceh Selatan', 'Aceh Singkil', 'Aceh Tamiang', 'Aceh Tengah', 'Aceh Tenggara', 'Aceh Timur', 'Aceh Utara', 'Bener Meriah', 'Bireuen', 'Gayo Lues', 'Nagan Raya', 'Pidie', 'Pidie Jaya', 'Simeulue', 'Banda Aceh', 'Langsa', 'Lhokseumawe', 'Sabang', 'Subulussalam');
			case 2:
				return array('Asahan', 'Batubara', 'Dairi', 'Deli Serdang', 'Humbang Hasundutan', 'Karo', 'Labuhan batu', 'Labuhan batu Selatan', 'Labuhan batu Utara', 'Langkat', 'Mandailing Natal', 'Nias', 'Nias Barat', 'Nias Selatan', 'Nias Utara', 'Padang Lawas', 'Padang Lawas Utara', 'Pakpak Bharat', 'Samosir', 'Serdang Bedagai', 'Simalungun', 'Tapanuli Selatan', 'Tapanuli Tengah', 'Tapanuli Utara', 'Toba Samosir', 'Binjai', 'Gunungsitoli', 'Medan', 'Padangsidempuan', 'Pematangsiantar', 'Sibolga', 'Tanjungbalai', 'Tebing Tinggi');
			case 3:
				return array('Agam', 'Dharmasraya', 'Kepulauan Mentawai', 'Lima Puluh Kota', 'Padang Pariaman', 'Pasaman', 'Pasaman Barat', 'Pesisir Selatan', 'Sijunjung', 'Solok', 'Solok Selatan', 'Tanah Datar', 'Bukit Tinggi', 'Padang', 'Padang Panjang', 'Pariaman', 'Payakumbuh', 'Sawahlunto', 'Solok');
			case 4:
				return array('Bengkalis', 'Indragiri Hilir', 'Indragiri Hulu', 'Kampar', 'Kuantan Singingi', 'Pelalawan', 'Rokan Hilir', 'Rokan Hulu', 'Siak', 'Kepulauan Meranti', 'Dumai', 'Pekanbaru');
			case 5:
				return array('Bintan', 'Karimun', 'Kepulauan Anambas', 'Lingga', 'Natuna', 'Batam', 'Tanjung Pinang');
			case 6:
				return array('Batanghari', 'Bungo', 'Kerinci', 'Merangin', 'Muaro Jambi', 'Sarolangun', 'Tanjung Jabung Barat', 'Tanjung Jabung Timur', 'Tebo', 'Jambi', 'Sungai Penuh');
			case 7:
				return array('Banyuasin', 'Empat Lawang', 'Lahat', 'Muara Enim', 'Musi Banyuasin', 'Musi Rawas', 'Ogan Ilir', 'Ogan Komering Ilir', 'Ogan Komering Ulu', 'Ogan Komering Ulu Selatan', 'Ogan Komering Ulu Timur', 'Lubuklinggau', 'Pagar Alam', 'Palembang', 'Prabumulih');
			case 8:
				return array('Bangka', 'Bangka Barat', 'Bangka Selatan', 'Bangka Tengah', 'Belitung', 'Belitung Timur', 'Pangkal Pinang');
			case 9:
				return array('Bengkulu Selatan', 'Bengkulu Tengah', 'Bengkulu Utara', 'Kaur', 'Kepahiang', 'Lebong', 'Mukomuko', 'Rejang Lebong', 'Seluma', 'Bengkulu');
			case 10:
				return array('Lampung Barat', 'Lampung Selatan', 'Lampung Tengah', 'Lampung Timur', 'Lampung Utara', 'Mesuji', 'Pesawaran', 'Pringsewu', 'Tanggamus', 'Tulang Bawang', 'Tulang Bawang Barat', 'Way Kanan', 'Bandar Lampung', 'Metro');
			case 11:
				return array('Kepulauan Seribu', 'Jakarta Barat', 'Jakarta Pusat', 'Jakarta Selatan', 'Jakarta Timur', 'Jakarta Utara');
			case 12:
				return array('Bandung', 'Bandung Barat', 'Bekasi', 'Bogor', 'Ciamis', 'Cianjur', 'Cirebon', 'Garut', 'Indramayu', 'Karawang', 'Kuningan', 'Majalengka', 'Purwakarta', 'Subang', 'Sukabumi', 'Sumedang', 'Tasikmalaya', 'Bandung', 'Banjar', 'Bekasi', 'Bogor', 'Cimahi', 'Cirebon', 'Depok', 'Sukabumi', 'Tasikmalaya');
			case 13:
				return array('Tangerang', 'Serang', 'Lebak', 'Pandeglang', 'Tangerang', 'Serang', 'Cilegon', 'Tangerang Selatan');
			case 14:
				return array('Banjarnegara', 'Banyumas', 'Batang', 'Blora', 'Boyolali', 'Brebes', 'Cilacap', 'Demak', 'Grobogan', 'Jepara', 'Karanganyar', 'Kebumen', 'Kendal', 'Klaten', 'Kudus', 'Magelang', 'Pati', 'Pekalongan', 'Pemalang', 'Purbalingga', 'Purworejo', 'Rembang', 'Semarang', 'Sragen', 'Sukoharjo', 'Tegal', 'Temanggung', 'Wonogiri', 'Wonosobo', 'Magelang', 'Pekalongan', 'Salatiga', 'Semarang', 'Surakarta', 'Tegal');
			case 15:
				return array('Bantul', 'Gunung Kidul', 'Kulon Progo', 'Sleman', 'Yogyakarta');
			case 16:
				return array('Bangkalan', 'Banyuwangi', 'Blitar', 'Bojonegoro', 'Bondowoso', 'Gresik', 'Jember', 'Jombang', 'Kediri', 'Lamongan', 'Lumajang', 'Madiun', 'Magetan', 'Malang', 'Mojokerto', 'Nganjuk', 'Ngawi', 'Pacitan', 'Pamekasan', 'Pasuruan', 'Ponorogo', 'Probolinggo', 'Sampang', 'Sidoarjo', 'Situbondo', 'Sumenep', 'Trenggalek', 'Tuban', 'Tulungagung', 'Batu', 'Blitar', 'Kediri', 'Madiun', 'Malang', 'Mojokerto', 'Pasuruan', 'Probolinggo', 'Surabaya');
			case 17:
				return array('Badung', 'Bangli', 'Buleleng', 'Gianyar', 'Jembrana', 'Karangasem', 'Klungkung', 'Tabanan', 'Denpasar');
			case 18:
				return array('Bima', 'Dompu', 'Lombok Barat', 'Lombok Tengah', 'Lombok Timur', 'Lombok Utara', 'Sumbawa', 'Sumbawa Barat', 'Bima', 'Mataram');
			case 19:
				return array('Alor', 'Belu', 'Ende', 'Flores Timur', 'Kupang', 'Lembata', 'Manggarai', 'Manggarai Barat', 'Manggarai Timur', 'Ngada', 'Nagekeo', 'Rote Ndao', 'Sabu Raijua', 'Sikka', 'Sumba Barat', 'Sumba Barat Daya', 'Sumba Tengah', 'Sumba Timur', 'Timor Tengah Selatan', 'Timor Tengah Utara', 'Kupang');
			case 20:
				return array('Bengkayang', 'Kapuas Hulu', 'Kayong Utara', 'Ketapang', 'Kubu Raya', 'Landak', 'Melawi', 'Pontianak', 'Sambas', 'Sanggau', 'Sekadau', 'Sintang', 'Pontianak', 'Singkawang');
			case 21:
				return array('Barito Selatan', 'Barito Timur', 'Barito Utara', 'Gunung Mas', 'Kapuas', 'Katingan', 'Kotawaringin Barat', 'Kotawaringin Timur', 'Lamandau', 'Murung Raya', 'Pulang Pisau', 'Sukamara', 'Seruyan', 'Palangka Raya');
			case 22:
				return array('Balangan', 'Banjar', 'Barito Kuala', 'Hulu Sungai Selatan', 'Hulu Sungai Tengah', 'Hulu Sungai Utara', 'Kotabaru', 'Tabalong', 'Tanah Bumbu', 'Tanah Laut', 'Tapin', 'Banjarbaru', 'Banjarmasin');
			case 23:
				return array('Berau', 'Kutai Barat', 'Kutai Kartanegara', 'Kutai Timur', 'Paser', 'Penajam Paser Utara', 'Balikpapan', 'Bontang', 'Samarinda');
			case 24:
				return array('Malinau', 'Bulungan', 'Nunukan', 'Tana Tidung', 'Tarakan');
			case 25:
				return array('Bolaang Mongondow', 'Bolaang Mongondow Selatan', 'Bolaang Mongondow Timur', 'Bolaang Mongondow Utara', 'Kepulauan Sangihe', 'Kepulauan Siau Tagulandang Biaro', 'Kepulauan Talaud', 'Minahasa', 'Minahasa Selatan', 'Minahasa Tenggara', 'Minahasa Utara', 'Bitung', 'Kotamobagu', 'Manado', 'Tomohon');
			case 26:
				return array('Majene', 'Mamasa', 'Mamuju', 'Mamuju Utara', 'Polewali Mandar');
			case 27:
				return array('Banggai', 'Banggai Kepulauan', 'Buol', 'Donggala', 'Morowali', 'Parigi Moutong', 'Poso', 'Tojo Una-Una', 'Toli-Toli', 'Sigi', 'Palu');
			case 28:
				return array('Bombana', 'Buton', 'Buton Utara', 'Kolaka', 'Kolaka Utara', 'Konawe', 'Konawe Selatan', 'Konawe Utara', 'Muna', 'Wakatobi', 'Bau-Bau', 'Kendari');
			case 29:
				return array('Bantaeng', 'Barru', 'Bone', 'Bulukumba', 'Enrekang', 'Gowa', 'Jeneponto', 'Kepulauan Selayar', 'Luwu', 'Luwu Timur', 'Luwu Utara', 'Maros', 'Pangkajene dan Kepulauan', 'Pinrang', 'Sidenreng Rappang', 'Sinjai', 'Soppeng', 'Takalar', 'Tana Toraja', 'Toraja Utara', 'Wajo', 'Makassar', 'Palopo', 'Parepare');
			case 30:
				return array('Boalemo', 'Bone Bolango', 'Gorontalo', 'Gorontalo Utara', 'Pohuwato', 'Gorontalo');
			case 31:
				return array('Buru', 'Buru Selatan', 'Kepulauan Aru', 'Maluku Barat Daya', 'Maluku Tengah', 'Maluku Tenggara', 'Maluku Tenggara Barat', 'Seram Bagian Barat', 'Seram Bagian Timur', 'Ambon', 'Tual');
			case 32:
				return array('Halmahera Barat', 'Halmahera Tengah', 'Halmahera Utara', 'Halmahera Selatan', 'Kepulauan Sula', 'Halmahera Timur', 'Pulau Morotai', 'Ternate', 'Tidore Kepulauan');
			case 33:
				return array('Asmat', 'Biak Numfor', 'Boven Digoel', 'Deiyai', 'Dogiyai', 'Intan Jaya', 'Jayapura', 'Jayawijaya', 'Keerom', 'Kepulauan Yapen', 'Lanny Jaya', 'Mamberamo Raya', 'Mamberamo Tengah', 'Mappi', 'Merauke', 'Mimika', 'Nabire', 'Nduga', 'Paniai', 'Pegunungan Bintang', 'Puncak', 'Puncak Jaya', 'Sarmi', 'Supiori', 'Tolikara', 'Waropen', 'Yahukimo', 'Yalimo', 'Jayapura');
			case 34:
				return array('Fakfak', 'Kaimana', 'Manokwari', 'Maybrat', 'Raja Ampat', 'Sorong', 'Sorong Selatan', 'Tambrauw', 'Teluk Bintuni', 'Teluk Wondama', 'Sorong ');
			default:
				return array();
		}
	}

	function saleStatusTeks($status) {

		switch ($status) {
			case 0:
				return 'BELUM KONFIRMASI';
				break;
			case 1:
				return 'KOMFIRMASI TERKIRIM';
				break;
			case 2:
				return 'KONFIRMASI DISETUJUI';
				break;
			case 3:
				return 'KONFIRMASI DITOLAK';
				break;
			case 4:
				return 'BARANG TERKIRIM';
				break;
			
			default:
				return $status;
				break;
		}
	}

	function shippingStatusTeks($status) {

		switch ($status) {
			case 0:
				return 'TUNGGU PEMBAYARAN';
				break;
			case 1:
				return 'TUNGGU PENGIRIMAN';
				break;
			case 2:
				return 'TERKIRIM';
				break;
			
			default:
				return $status;
				break;
		}
	}




?>