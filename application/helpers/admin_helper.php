<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	
	define('STYLE_VERS',1);
	
	function statusTeks($status) {

		switch ($status) {
			case ADD_SUCCEED:
				return 'DATA ADDED SUCCESSFULLY';
				break;
			case ADD_FAILED:
				return 'DATA ADD FAILED';
				break;
			case UPDATE_SUCCEED:
				return 'DATA UPDATED SUCCESSFULLY';
				break;
			case UPDATE_FAILED:
				return 'DATA UPDATE FAILED';
				break;
			case DATA_NOT_FOUND:
				return 'DATA NOT FOUND';
				break;
			case LOGIN_FAILED:
				return 'LOGIN FAILED';
				break;
			case DELETE_SUCCEED:
				return 'DATA DELETED SUCCESSFULLY';
				break;
			default:
				return NULL;
				break;
		}
	}
	
	
	function printNotice($status)
	{
		?>
		<div class="notice-box"><?= statusTeks($status); ?></div>
		<?php
	}
