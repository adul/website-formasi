<html>
<head>
	<title>Upload Form</title>
</head>
<body>

	<p>
		<a href="<?= INDEX_URL . 'mailing' ?>"><< Kembali</a>
	</p>

	<?php 
	if (isset($tpl['error'])) {
		echo "<strong> ERROR: " .  $tpl['error']['error'] . "</strong>"; 	
	} else {
		$arr_email = $tpl['arr_email'];
		echo form_open_multipart('mailing/proses_file');
	?>
	<div>
		<label>Pilih Jenis Email:</label>
		<select name="email_id">
			<?php foreach ($arr_email as $key => $value) 
			{ ?>
				<option value="<?= $value->id ?>" ><?= $value->judul ?></option>
				<?php
			} ?>
		</select>
	</div>
	<br /><br />
	<div>
		<label>UPLOAD File Excel data pelanggan (*.csv)</label>
		<br />
		<label>simpan file excel ke bentuk csv, <br /> data yang dibaca adalah mulai dari baris ke dua, <br /> format file harus yang sudah ditentukan </label>
		<br />
		<br />
		<input type="file" name="userfile" size="20" />
	</div>
	<br />
		<br />
	<input type="submit" value="PROSES" />

</form>
<?php
} ?>
</body>
</html>