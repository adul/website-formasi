

<?php 
$data = $tpl['data'];
$indBulan = $tpl['indBulan'];

?>
	
	<div id = "row1">
    	<div  style="width:380px; float:left; margin-top:40px;">Setelah file di download, extract/unzip terlebih dahulu file-nya untuk dapat digunakan</div>
        <div  style="float:right; cursor:pointer;"><img src="<?= IMG_PATH ?>balon.png" width="200" height="100" id = "balon"></div>

        <p style="clear:both;"></p>          
                    
        <div id = "row2">
            <div id="Guide">
            	<div style="margin-bottom:20px;">
                	<P>File yang di-download sudah mencakup semua file (peraturan, lampiran & formulir)</P>
					<p>Setelah file di download, extract/ unzip dahulu file sebelum digunakan. File ini akan menghasilkan sekumpulan file-file ber-ekstensi .lmf (gambar 1)</p>
              	</div>
                        
                        
                        
			<div id = "GambarUpdateTaxes1"><img src="<?= IMG_PATH ?>UpdateTaxes1.png" width="400" height="240"></div>
                        
                        <div id = "KetGbrUpdateTaxes">
                            <p>Keterangan:</p>
                            <ol style="margin-left:20px;">
                                <li>Alamat Folder tempat file di-exstact.</li>
                                <li>File asli yang di-download dari situs ini.</li>
                                <li>File-file hasil exstract file.</li>
                            </ol>
                        </div>
                        
                        
                        <p style="clear:both;"></p>
                        
                        <p style="margin-top:20px;">Setelah file di-extract, silahkan buka aplikasi Taxes 2.0. anda dan lakukan langkah-langkah update database berikut:</p>
                        <br />
                        
                          <div id = "GambarUpdateTaxes2"><img src="<?= IMG_PATH ?>UpdateTaxes2.png" width="400" height="470"></div>
                        <div>
                            <ol style="margin-left:20px;">
                                <li>Klik menu update pada toolbar atau pilih menu Tools & Setting => Update Database</li>
                                <li>Pada jendela yang muncul, pilih ‘DARI FOLDER‘</li>
                                <li>Pilih Drive dimana anda menyimpan file-file hasil extract</li>
                                <li>Lanjutkan dengan memilih alamat folder file-file tersebut</li>
                                <li>Jika file-file pada folder ditemukan, akan ditampilkan pada kotak di kanan</li>
                                <li>Klik ‘OK‘ untuk melanjutkan</li>
                                <li>Mulai proses update, klik tombol ‘START‘ dan tunggu hingga muncul pesan bahwa proses selesai</li>
                                <li>Pilih ‘Lampiran‘ dan klik  ‘START‘ seperti pada langkah 7 dan ulangi juga untuk ‘Formulir‘</li>
                             </ol>   
                         </div>    
                  </div>
                
            </div>
           <p style="clear:both;"></p>          
                    
      </div>
            
            
            <div style="margin-left:30px; margin-top:20px;">
            <?php

			if ($data == false) 
			{
				echo '<div style="color: red;font-size: 14px;font-weight: bold">Maaf belum ada update pada bulan dan tahun yang anda dipilih</div>';
			} else {
				foreach($data as $value)
				{  ?>
			</div>	
				
				<div id = "row3">
					<div style="width:300px; float:left;">
						<p style="font-size:18px;"><?= $indBulan[$value->bulan] . ' '.  $value->tahun . ' [File ke-' . $value->edisi . ']' ?></p>
						<p style="font-size:12px;">File type: zip</p>
						<p style="font-size:12px;">Size: <?= $value->ukuran ?> bytes</p>
					</div>
					
					<div style="float:right;">
					  <div class = "BtnUnduh"><a href="http://formasi.com/taxes_update/zipfiles/<?= $value->bulan . '-' . $value->tahun . '-' . $value->edisi.'.zip' ?>">Unduh</a></div>
					  <div class = "BtnDetail" rel ="<?= $value->id ?>">Detail</div>
					</div>
                	<p style="clear:both;"></p>
                
                
                	<div class= "row_detail" id = "row_detail_<?= $value->id ?>">
                		<?= $value->ditail ?>
            		</div>
                </div>

                <?php
				}
				
			}
			?>
			
            
           
            
            
            