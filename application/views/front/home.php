<div id="bg-title">
       <div id="image-banner"><img src="../../../public/img/image-banner-2.png" /></div>
</div>   
		
<div id="deskripsi">
    <table width="960" style="border-collapse:collapse; border-left:1px solid #000;">
        <tr>
            <td width="320" align="center" style="border-right:1px solid #000000;"><span id="sub-judul">Apa Itu <br /> Reseller Pustaka Lebah?</span></td>
            <td width="320" align="center" style="border-right:1px solid #000000;"><span id="sub-judul">Apa Manfaat <br /> 
            Reseller Pustaka Lebah?</td>
            <td width="320" align="center" style="border-right:1px solid #000000;"><span id="sub-judul">Bagaimana <br /> 
            Caranya?</td>
        </tr>
        <tr>
            <td align="center" style="border-right:1px solid #000000;"><img src="../../../public/img/logo-reseller.png" width="200"  /></td>
            <td align="center" style="border-right:1px solid #000000;"><img src="../../../public/img/profit.png" width="240"  /></td>
            <td align="center" style="border-right:1px solid #000000;"><img src="../../../public/img/key.png" width="240"  /></td>
        </tr>
        <tr>
            <td align="center" style="border-right:1px solid #000000;"><button id="btn-des-reseller">Baca</button></td>
            <td align="center" style="border-right:1px solid #000000;"><button id="btn-manfaat-reseller">Baca</button></td>
            <td align="center" style="border-right:1px solid #000000;"><button id="btn-cara-reseller">Baca</button></td>
        </tr>
    </table>
                
    <br />
        
    <div id="deskripsi-reseller">
        <div id="deskripsi-close">X</div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <div id="deskripsi-text">
            <h2 style="color:#606060; text-indent:0px;">Sukses Menjadi Reseller Pustaka Lebah</h2>
            <br />
            <br />
            
            <p>Setiap orang memiliki keinginan memiliki usaha sendiri yang dapat dijadikan sandaran untuk menopang pemenuhan kebutuhan sehari-harinya. Terkendala oleh persyaratan modal yang cukup besar, jangka waktu perolehan keuntungan yang tidak pasti, serta ditambah lagi kemungkinan terjadinya resiko usaha (kerugian) yang selalu mengintai, seseorang seringkali harus membatalkan niatnya, bahkan sebelum memulainya sama sekali.</p>
            
            <br />
            
            <p>Menjadi seorang reseller dapat menjadi solusi yang aman dan sangat menguntungkan bagi mereka yang sedang menata langkah dalam dunia usaha. Untuk menjadi seorang reseller yang berhasil tidak diperlukan modal besar. Resiko kerugian yang harus dihadapi seorang reseller juga relatif kecil. Bagaimanapun, hal ini tidak terlepas dari dukungan produk yang mudah dijual (dibutuhkan oleh banyak orang) serta kiat-kiat tertentu dalam pengelolaan usaha dan langkah-langkah antisipatif terhadap kemungkinan terjadinya resiko yang tidak diharapkan.</p>   
         
            <br />
               
            <p>Anak-anak adalah aset masa depan yang membutuhkan banyak bekal agar dalam pertumbuhannya, mereka memiliki kesiapan baik moral maupun intelektual. Selain melalui jalur pendidikan formal, anak-anak juga harus diberikan bahan referensi pendukung, seperti misalnya buku-buku dan bahan bacaan yang berguna dan akan memperkaya wawasan berpikir mereka. Buku-buku tersebut harus dikemas sedemikian rupa agar tersaji menarik dan dapat menumbuhkan minat baca pada anak-anak. Orangtua sekarang kebanyakan telah menyadari hal ini sehingga mereka berusaha sekuat tenaga untuk mendapatkan buku pendidikan yang terbaik bagi anak-anaknya.</p>
            
            <br />
            
            <p>Di sini terlihat bahwa buku pendidikan anak-anak yang baik sudah menjadi kebutuhan yang dicari-cari oleh banyak orangtua. Namun untuk memperoleh buku-buku yang baik, para orangtua kebanyakan terkendala oleh kesibukan pekerjaan mereka. Mereka tidak memiliki cukup waktu untuk "berburu" buku bermutu yang dibutuhkan dan menarik bagi anak-anaknya. Dengan kondisi demikian, kira-kira, apa yang terjadi apabila buku-buku yang baik dan menarik justru mendatangi para orangtua dan mereka yang tidak mempunyai cukup waktu untuk "berburu"? Benar...inilah peluang yang dimiliki oleh seorang reseller buku anak. Lalu bagaimana dengan keuntungan bagi seorang reseller buku anak? Produk buku anak seperti apa yang memiliki nilai jual tinggi, dan mengapa demikian? Bagaimana aturan main atau sistem yang berlaku untuk menjadi seorang reseller buku anak? Silakan baca di manfaat dan cara menjadi reseller Pustaka Lebah.</p>
        
        </div> 
    </div>
    
    <div id="manfaat-reseller">
        <div id="manfaat-close">X</div>
            <br />
            <br />
            <br />
            <br />
        <div id="deskripsi-text">
            <h2 style="color:#606060; text-indent:0px;">Manfaat Menjadi Reseller Pustaka Lebah</h2>
            <br />
            <br />
            
            <p>Banyak manfaat dan keuntungan dapat diperoleh dengan mengikuti program ini, yaitu:</p>
           
            <br />
             
            <ul style="list-style:circle; margin-left:20px;">
                <li>Memberikan bacaan yang bermanfaat kepada anak-anak selama Bulan Ramadhan.</li>
                <li>Mengajak anak-anak untuk lebih mengenal Nabi Muhammad Saw.</li>
                <li>Menanamkan nilai-nilai luhur kepada anak-anak dengan mengambil teladan yang diberikan oleh Nabi Muhammad Saw.</li>
                <li>Memperluas relasi dan/atau jaringan sosial yang bersifat mutualistis (saling menguntungkan)</li>
                <li>Mendapatkan buku yang berkualitas dengan harga sangat murah (diskon hingga 70%), sehingga mudah untuk dijual kembali dengan keuntungan cukup besar.</li>
                <li>Memberikan kegiatan positif yang dapat dijadikan peluang usaha untuk menambah penghasilan keluarga.</li>
                <li>Memberikan kegiatan yang produktif dengan waktu yang dapat diatur secara fleksibel sehingga tidak mengganggu kegiatan/rutinitas lainnya.</li>
                <li>Proses untuk bergabung menjadi reseller sangat mudah.</li>
                <li>Tersedia pilihan paket program yang sesuai dengan alokasi modal yang anda ditetapkan.</li>
            </ul>
            
        </div>
    </div>
    
    
    <div id="cara-reseller">
        <div id="cara-close">X</div>
        tiga
    </div>


</div>


<div id="bg-produk">
<div id="buku">
    <img src="../../../public/img/buku.png" />
    <div id="button-produk">Lihat Semua Produk</div>
</div>
</div>