<div id="container-produk">
	<?php for ($i=1; $i<=12; $i++)
    { ?>
    <div class="tgpm-box">
    	<div class="img-tgpm">
    		<img src="../../../public/img/tgpm/sirah buku <?= $i ?>.jpg" width="230" class="display-tgpm" id="<?= $i ?>"  />
        </div>
        <br />
        <table style="cursor:pointer; color:#D77427">
            <tr>
              <td style="text-align: center; border-right:1px solid #D77427; width:150px;" class="prev-text" id="<?= $i ?>">Preview</td>
              <td style="text-align: center; width:150px;">Order</td>
            </tr>
		</table>
    </div>
    <?php
	}
	?>
</div>

<div id="tgpm-preview">
	<div id="btn-close"><img src="../../../public/img/close_button.png" width="40" /></div>
	<div id="img-detail"><img src="" id="img-preview" width="330" /></div>
    <div id="book-desk"></div>
    <div id="btn-order"><img src="../../../public/img/btn-order.png" width="200"/></div>
</div>

<div id="book-detail" style="display:none;">  
	<div id="judul1">
    	<h1>Masyarakat Jahiliah</h1>
        <br />
        <p>Pada masa jahiliah, hampir semua masyarakat di seluruh negeri telah jauh dari petunjuk Allah Swt. Mereka biasa berjudi, minum minuman keras, bahkan menyekutukan Allah dengan berhala, bintang, dan api. Allah telah mengutus nabi untuk menunjukkan jalan yang benar kepada mereka. Nabi Muhammad Saw, terpilih sebagai nabi terakhir untuk kembali memberi petunjuk bagi seluruh umat manusia di dunia.</p>
	</div>
    <div id="judul2">
    	<h1>Masa Kecil</h1>
        <br />
        <p>Saat kecil, Nabi Muhammad Saw. tinggal di perkampungan Bani Sa’ad, dalam asuhan keluarga Halimah binti Abi Dzuaib. Perkampungan Bani Sa’ad memiliki udara yang bersih dan wilayah yang berbukit-bukit sehingga fisik Muhammad kecil tumbuh kuat dan sehat. Bani Sa’ad juga menggunakan bahasa arab yang fasih. Lingkungan masa kecil Nabi turut mempersiapkan beliau untuk memimpin perubahan besar di jazirah Arab.</p>
	</div>
    <div id="judul3">
    	<h1>Masa Remaja</h1>
        <br />
        <p>Muhammad muda menggembalakan domba saat remaja. Kebiasaan itu melatih kemandirian dalam dirinya. Jiwa sosial Nabi juga terasah ketika terlibat Perang Fijar dan Hilfal Fudhul. Selain itu, tanda-tanda kenabian telah terlihat oleh pendeta Buhaira saat Muhammad muda bersama kafilah Abu Thalib memasuki Busra. Tanda-tanda itu sesuai dengan tanda-tanda dalam kitab-kitab suci terdahulu.</p>
	</div>
    <div id="judul4">
    	<h1>Orang Terpercaya</h1>
        <br />
        <p>Sebagai wanita pilihan Allah Swt. untuk mendampingi Nabi Muhammad Saw., Khadijah binti Khuwailid merupakan wanita yang Allah jaga garis keturunan dan akhlaknya. Pernikahan Nabi Muhammad dengan Khadijah menjadi teladan umat Islam dalam membina keluarga. Bersama Khadijah, Nabi Muhammad menjadi orang terpercaya di tengah-tengah masyarakat Quraisy.</p>
	</div>
    <div id="judul5">
    	<h1>Generasi Islam Pertama</h1>
        <br />
        <p>Setelah turun wahyu pertama, Allah Swt menunda turunnya wahyu selama beberapa bulan. Nabi Muhammad Saw. sempat merasa sedih dan gelisah. Penantian Rasulullah tersebut usai dengan wahyu kedua yang memerintahkan Nabi Muhammad untuk mulai berdakwah. Nabi Muhammad pun mulai berdakwah pada kerabat, tetangga, dan orang terdekat. Selanjutnya Nabi Muhammad membina generasi Islam pertama di Darul Arqam.</p>
	</div>
    <div id="judul6">
    	<h1>Dakwah Terbuka</h1>
        <br />
        <p>Nabi Muhammad Saw. mendapat perintah berdakwah secara terbuka. Rasulullah berdakwah kepada seluruh penduduk Mekah dan keluarga besarnya. Dakwah terbuka itu tidak mudah. Berbagai gangguan dan penyiksaan menimpa Nabi dan para sahabat. Bahkan, kebutuhan sandang-pangan kaum muslim diboikot. Karena itulah, Rasulullah memerintahkan beberapa sahabat berhijrah ke Habasyah dan mulai berdakwah di luar Mekah.</p>
	</div>
    <div id="judul7">
    	<h1>Hijrah</h1>
        <br />
        <p>Rasulullah meluaskan dakwahnya ke luar Mekah. Ternyata penduduk Yatsrib (setelah peristiwa Hijrah, Yatsrib kemudian disebut Madinah) menyambut baik dakwah Islam tersebut. Nabi Muhammad meminta janji setia muslimin Yatsrib melalui Baiat Aqabah. Setelah Bait Aqabah I dan II, Nabi Muhammad beserta para sahabat hijrah untuk membangun Madinah berdasarkan tuntunan Islam.</p>
	</div>
    <div id="judul8">
    	<h1>Pembebasan Mekah</h1>
        <br />
        <p>Disepakatinya Perjanjian Hudaibiyah menciptakan perdamaian antara pihak Quraisy dan umat Islam. Sayangnya, perjanjian itu dilanggar Quraisy yang membantu bani Bakar menyerang bani Khuza’ah. Bani Khuza’ah melaporkan hal itu kepada Nabi Muhammad Saw. Nabi Muhammad kemudian memimpin pasukan muslimin menuju Mekah untuk menaklukkan Mekah secara damai.</p>
	</div>
    <div id="judul9">
    	<h1>Wafatnya Rasulullah</h1>
        <br />
        <p>Saat haji wada, Allah menurunkan wahyu terakhir yang menyatakan ajaran Islam telah sempurna. Sekembali dari haji wada, Nabi Muhammad sakit hingga akhirnya menghembuskan napas terakhir. Wafatnya Nabi Muhammad meninggalkan duka mendalam bagi para sahabat. Para sahabat kemudian bermusyawarah untuk memilih pengganti beliau memimpin umat Islam.</p>
	</div>
    <div id="judul10">
    	<h1>Keluarga Rasulullah</h1>
        <br />
        <p>Nabi Muhammad Saw. baru menikah lagi setelah 25 tahun hidup bersama Khadijah binti Khuwailid. Sepeninggal Khadijah pun, Nabi menikah dengan beberapa wanita karena berbagai alasan. Misalnya untuk perluasan dakwah Islam. Para istri dan anak-cucu Nabi Muhammad memberikan contoh kepada umat Islam bagaimana menyelesaikan masalah dalam kehidupan berkeluarga.</p>
	</div>
    <div id="judul11">
    	<h1>Teladan Rasulullah</h1>
        <br />
        <p>Meskipun Allah Swt. memilihnya sebagai nabi terakhir yang menyempurnakan agama tauhid, Nabi Muhammad Saw. tetap hidup sederhana dan rendah hati. Nabi Muhammad juga dikenal dengan keberanian dan keteguhan hatinya saat menghadapi orang-orang munafik dan musuh-musuh Islam. Nabi Muhammad juga dikenal dengan sifatnya yang murah hati dan pemaaf kepada orang lain.</p>
	</div>
    <div id="judul12">
    	<h2>Penerus Kepemimpinan Rasulullah</h2>
        <br />
        <p> Abu Bakar ash-Shiddiq, Umar bin Khattab, Utsman bin Affan, dan Ali bin Abi Thalib secara bergantian dipercaya umat Islam sebagai penerus kepemimpinan Nabi Muhammad Saw. setelah beliau wafat. Mereka memiliki sifat, kekuatan, dan keunikan yang berbeda, namun Islam berhasil menyatukan hati mereka. Di bawah kepemimpinan mereka Islam semakin berkembang luas.</p>
	</div>
</div>


<div id="shadow"></div>