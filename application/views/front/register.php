<div id="bg-title">
     <div id="reg-title">
     	<h1>FORMULIR PENDAFTARAN RESELLER</h1>
     </div>
</div> 

<br />
<br />

<div id="container-form">

    <div id="logo-form">
    	<img src="../../../public/img/logo-form.png" >
    </div>
    
    <div id="box-form">
          <form class="form" style="width: 860px;margin: auto" action="<?= base_url('member/register') ?>" method="post"  >
              <div class="frame" >
          
                  <input type="hidden" name="register" value="1" />
                          <div class="p">
                              <label class="title" >Kode AE</label> <br />
                              <input name="ae_id" type="text" class="width-fill" value="<?= set_value('ae_id') ?>" />
                              <?= form_error('ae_id') ?>
                  
                          </div>
          
                  <div class="p">
                      <label class="title" >Nama Lengkap</label> <br />
                      <input name="name" type="text" class="width-fill" value="<?= set_value('name') ?>" />
                      <?= form_error('name') ?>
          
                  </div>
          
                  <div class="p">
                      <label class="title" >Alamat</label> <br />
                      <textarea name="address" class="width-fill" style="width:350px; height:150px;" ><?= set_value('address') ?></textarea>
                      <?= form_error('address') ?>
                  </div>
          
                  <div class="p">
                      <label class="title" >Kab/Kota</label> <br />
                      <input name="city" type="text" class="width-fill" value="<?= set_value('city') ?>" /><br /> 
                      
                      <div id="provinsi">
                          <label  class="title">Provinsi</label> <br /> 
                          <select name="province" class="width-fill" style="width:250px;"  >
                              <option value="" >-- PILIH --</option>
                              <?php
                              for ($i=1; $i <= 33 ; $i++) { ?>
                                  <option value="<?= $i ?>" <?= set_select('province', $i) ?> ><?= provinceTeks($i) ?></option>
                                  <?php
                              } ?>
                          </select>
                      </div>
                      
                      <div id="kode-post">
                            <label class="title" style="margin-left:20px;">Kode Pos</label><br />
                            <input name="zip" type="text" class="width-fill" style="width:80px; margin-left:20px;" value="<?= set_value('zip') ?>" />
                            <?= form_error('city') ?>
                            <?= form_error('province') ?>
                            <?= form_error('zip') ?>
          			</div>
          
                  </div>
  					
                    <p style="clear:both;"></p>
                            
                  <div class="p">
                      
                      <div id="telp">
                            <label class="title">Telp/HP</label><br />
                            <input name="phone" type="text" class="width-fill" style="width:250px;" value="<?= set_value('phone') ?>"  />
                      </div>
                      
                      <div id="bb-pin">
                            <label class="title" style="margin-left:20px;">Pin BB</label> <br />
                            <input name="bb_pin" type="text" class="width-fill"  style="width:80px; margin-left:20px;" value="<?= set_value('bb_pin') ?>" />
                            <?= form_error('phone') ?>
                      </div>
          
                  </div>
                  	
                    <p style="clear:both;"></p>
                  
                  <div class="p">
                      <label class="title" >Email</label><br />
                      <input name="email" type="text" class="width-fill" style="margin-bottom:15px;" value="<?= set_value('email') ?>" /><br />
                      <label class="title">Email-2</label><br />
                      <input name="email_2" type="text" class="width-fill" style="margin-bottom:15px;" value="<?= set_value('email_2') ?>" /><br />
                      <label class="title">Email-3</label><br />
                      <input name="email_3" type="text" class="width-fill" value="<?= set_value('email_3') ?>" />
                      <?= form_error('email') ?>
          
                  </div>
                  <div class="p">
                      <label class="title" >Password</label><br />
                      <input name="password" type="password" class="width-fill" value="" />
                      <?= form_error('password') ?>
                  </div>
                  <div class="p" >
                      <label class="title" >&nbsp;</label>
                      <input type="checkbox" name="have_read" id="have_read"  />
                      <label for="have_read" >Saya telah membaca dan menyetujui syarat, ketentuan dan kebijakan privasi</label>
                      <?php if (form_error('have_read') != NULL ) {?> 
                          <div class="error_note">
                              * syarat, ketentuan dan kebijakan privasi harus disetujui 
                          </div>
                          <?php
                      } ?>
          
                  </div>
                  <div class="p">
                      <label class="title" >&nbsp;</label>
                      <label class="lbl_disclaimer" rel="term" style="margin-right: 40px" >Baca Syarat dan Ketentuan</label>
                      <label class="lbl_disclaimer" rel="privacy">Baca Kebijakan Privasi</label>
                  </div>
                  <br />
                  <p >
                      <input class="submit-reg" type="submit" value="DAFTAR" >
                  </p>
              </div>
              
              <br />
              <br />
              <br />
          </form>
    </div>
    
<p style="clear:both;"></p>
</div>