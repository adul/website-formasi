<div id="message" >
	<div id="msg-title" style="font-size: 18px; font-weight: bold; margin-bottom: 10px;" >
		<?= $msg_title ?>
	</div>

	<div id="msg-body" >
		<?= $msg_body ?>
	</div>
</div>