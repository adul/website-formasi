<div class="framee" id="shipping_box_<?= $shipping_number ?>" >
		<input type="hidden" name="shipping_number[]" value="<?= $shipping_number ?>" > 

		<div style="position: relative" >
			<div class="p" style="float: left;width: 50%">
				<label class="legend" >Alamat Pengiriman </label>
			</div>
			<div class="btnDelShipping" rel="<?= $shipping_number ?>" style="float:right;text-align:center;">
				HAPUS PENGIRIMAN INI
			</div>
		</div>
        
        <p style="clear:both;"></p>
        
         <br />
         <br />
         
		<div class="p">
			<label class="title" >Pengiriman:</label>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" id="shipping_type_courier_<?= $shipping_number ?>" class="shipping_type" name="shipping_type_<?= $shipping_number ?>" rel="<?= $shipping_number ?>" value="courier" <?= set_radio('shipping_type_' . $shipping_number ,'courier',true) ?> >
			<label for="shipping_type_courier_<?= $shipping_number ?>">Kirim ke Alamat</label>&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="radio" id="shipping_type_house_<?= $shipping_number ?>" class="shipping_type" name="shipping_type_<?= $shipping_number ?>" rel="<?= $shipping_number ?>" value="house" <?= set_radio('shipping_type_' . $shipping_number ,'house') ?> />
			<label for="shipping_type_house_<?= $shipping_number ?>">Ambil ke Gudang PL</label>
		</div>
        
         <br />
         <br />
        
		<div id="shipping_courier_<?= $shipping_number ?>" style="display: <?= (set_value('shipping_type') == 'house') ? 'none' : 'block' ?>" >
			<div class="p">
				<label class="title" >Penerima:</label> <br />
				<input name="shipping_receiver_<?= $shipping_number ?>" type="text" class="shipping-fill" value="<?= ($post) ? set_value('shipping_receiver_' . $shipping_number) : NULL  ?>" />

				<?= form_error('shipping_receiver_' . $shipping_number) ?>
			</div>

			<div class="p">
				<label class="title" >Alamat:</label> <br />
				<textarea name="shipping_address_<?= $shipping_number ?>" class="shipping-fill" style="height:200px;" ><?= ($post) ? set_value('shipping_address_' . $shipping_number) : NULL  ?></textarea>
				<?= form_error('shipping_address_' . $shipping_number) ?>
			</div>


			<div class="p">
				<label class="title" >Provinsi:</label> <br />
				<select name="shipping_province_<?= $shipping_number ?>" class="shipping_province" rel="<?= $shipping_number ?>" >
					<option value="" >-- PILIH --</option>
					<?php
					for ($i=1; $i <= 33 ; $i++) { ?>
						<option value="<?= $i ?>" <?= ($post) ? set_select('shipping_province_' . $shipping_number, $i ) : NULL  ?> ><?= provinceTeks($i) ?></option>
						<?php
					} ?>
				</select>
				<?= form_error('shipping_province_' . $shipping_number) ?>
			</div>
			<div class="p">
				<label class="title" >Kab/Kota:</label> <br />
				<select id="shipping_city_<?= $shipping_number ?>" name="shipping_city_<?= $shipping_number ?>" class="shipping_city" >

				</select>
				<?= form_error('shipping_city_' . $shipping_number) ?>
			</div>

			<div class="p">
				<label class="title" >Kode Pos:</label> <br />
				<input name="shipping_zip_<?= $shipping_number ?>" type="text" class="shipping-fill" value="<?= ($post) ? set_value('shipping_zip_' . $shipping_number) : NULL  ?>" />
				<?= form_error('shipping_zip_' . $shipping_number) ?>
			</div>
			<div class="p">
				<label class="title" >Telp/HP:</label> <br />
				<input name="shipping_phone_<?= $shipping_number ?>" type="text" class="shipping-fill" value="<?= ($post) ? set_value('shipping_phone_' . $shipping_number) : NULL  ?>" />
				<?= form_error('shipping_phone_' . $shipping_number) ?>
			</div>
		</div>

		<div class="p">
			<div style="float: left">
				<label class="title" >Jumlah Paket:</label> <br />
				<input type="number" id="shipping_qty_<?= $shipping_number ?>" name="shipping_qty_<?= $shipping_number ?>" class="w50 input-number shipping_qty" style="width:100px; height:35px; border-radius:5px; border:1px solid #E5CAA7;" value="<?= ($post) ? set_value('shipping_qty_' . $shipping_number) : 1  ?>"  > 
				<?= form_error('shipping_qty_' . $shipping_number) ?>
			</div>

			<div style="float: right; width: 400px;position: relative">
				<label class="title" >Ongkos Kirim:</label> <br />
				<input type="text" readonly id="shipping_cost_<?= $shipping_number ?>" name="shipping_cost_<?= $shipping_number ?>" value="0" style="text-align:right; width:150px;" class="shipping_cost" >
				<img id="loader_<?= $shipping_number ?>" src="<?= base_url() . 'public/img/loading.gif' ?>" style="position: absolute; bottom: 0;right: 80px;height: 25px;display: none" >
			</div>
		</div>
        
        
        <br />
        <br />
        <br />
        <br />
        
        
	</div>
    