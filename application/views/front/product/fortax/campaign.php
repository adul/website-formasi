<style type="text/css">
	.w600 { width: 600px}
	.cbutton div {
		border: 1px solid rgb(0, 127, 255);
		width: 200px;
		border-radius: 5px;
		height: 50px;
		vertical-align: middle;
		line-height: 50px;
		background-color: rgb(0, 127, 255);
		color: white;

	}
	.cbutton a {
		text-decoration: none;
	}
</style>
<div align="center">
	<br>
	<br>
	<br>
	<div class="w600">
		<span style="font-size:18px;color:#008080;">
			<strong>
				<span style="font-family: arial, sans-serif; text-align: left;">Apakah Anda mengalami masalah-masalah ini?</span>
			</strong>
		</span>
	</div>
	<div class="w600">
		<ol>
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<h4 style="margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 16px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;color: #808080 !important;" class="null">
					<span style="font-size:12px;"><span style="color:#000000;">Bingung menentukan transaksi yang harus dikenakan pajak?</span></span></h4>
			</li>
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<h4 style="margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 16px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;color: #808080 !important;" class="null">
					<span style="font-size:12px;"><span style="color:#000000;">Salah memperhitungkan pajak?</span></span></h4>
			</li>
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<h4 style="margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 16px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;color: #808080 !important;" class="null">
					<span style="font-size:12px;"><span style="color:#000000;">Terlambat menyetor/lapor pajak?</span></span></h4>
			</li>
		</ol>
	</div>

	<div class="w600">
		<span style="font-size:18px;color:#008080;">
			<strong>
				<span style="font-family: arial, sans-serif; text-align: left;">Kini Anda tidak perlu khawatir, karena sudah ada solusinya!</span>
			</strong>
		</span>
	</div>
	<div class="w600">
		<ol>
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<h4 style="margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 16px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;color: #808080 !important;" class="null">
					<span style="font-size:12px;"><span style="color:#000000;"><strong>Mudah memahami transaksi yang harus dikenakan pajak dengan modul pembelajaran yang mudah dan cepat dipahami</strong></span></span></h4>
			</li>
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<h4 style="margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 16px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;color: #808080 !important;" class="null">
					<span style="font-size:12px;"><span style="color:#000000;"><strong>Hindari salah hitung pajak dengan belajar menghitung pajak yang benar disertai contoh studi kasus di lapangan</strong></span></span></h4>
			</li>
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<h4 style="margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 16px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;color: #808080 !important;" class="null">
					<span style="font-size:12px;"><span style="color:#000000;"><strong>Hindari terlambat setor/lapor pajak dengan mempelajari masa setor/lapor pajak sesuai dengan ketentuan yang berlaku</strong></span></span></h4>
			</li>
		</ol>

	</div>

	<div class="w600">
		<span style="font-size:18px;color:#008080;">
			<strong>
				<span style="font-family: arial, sans-serif; text-align: left;">Temukan semua solusi tersebut dalam satu langkah!</span>
			</strong>
		</span>
	</div>
	<br />
	<div class="w600">
		<img src="http://formasi.com/images/fortax/mtt_front.jpg">
	</div>
	<div class="w600">
		<span style="font-size:18px;color:#008080;">
			<strong>
				<span style="font-family: arial, sans-serif; text-align: left;">Apa saja keunggulan FORTAX? </span>
			</strong>
		</span>
	</div>
	<div class="w600" >
		<ol>
			<li style="text-align: left;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<span style="color:#000000;"><strong><span style="font-size:12px;">Mudah dan cepat dipahami</span></strong></span></li>
			<li style="text-align: left;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<span style="color:#000000;"><strong><span style="font-size:12px;">Penyajian yang terstruktur sehingga mudah dipahami bagi pemula maupun profesional</span></strong></span></li>
			<li style="text-align: left;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<span style="color:#000000;"><strong><span style="font-size:12px;">Praktis tidak terikat dengan waktu tertentu,dapat dilakukan berulang-ulang dan bersama-sama</span></strong></span></li>
			<li style="text-align: left;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<span style="color:#000000;"><strong><span style="font-size:12px;">Lengkap, berisi tentang konsep dan pengertian, cara dan contoh penghitungan pajak, dan&nbsp;Tax planning</span></strong></span></li>
		</ol>
	</div>
	<div class="w600">
		<span style="font-size:18px;color:#008080;">
			<strong>
				<span style="font-family: arial, sans-serif; text-align: left;">Apa saja yang bisa Anda lakukan dengan FORTAX? </span>
			</strong>
		</span>
	</div>
	<div class="w600">
		<ol style="text-align:left">
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<span style="color:#000000;"><span style="font-size:12px;"><strong>Memilih fitur dan konten dalam satu SMART MENU</strong></span></span></li>
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<span style="color:#000000;"><span style="font-size:12px;"><strong>Mempelajari seluk-beluk pajak dengan TRAINING MULTIMEDIA</strong></span></span></li>
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<span style="color:#000000;"><span style="font-size:12px;"><strong>Menampilkan konten dengan leluasa lewat MULTITAB</strong></span></span></li>
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<span style="color:#000000;"><span style="font-size:12px;"><strong>Mencari Peraturan Pajak dengan SMART SEARCH</strong></span></span></li>
			<li style="-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
				<span style="color:#000000;"><span style="font-size:12px;"><strong>Membaca Peraturan Pajak dengan SMART READ&nbsp;</strong></span></span></li>
		</ol>
	</div>
	<div class="w600">
		<span style="font-size:18px;color:#008080;">
			<strong>
				<span style="font-family: arial, sans-serif; text-align: left;">Informasi lebih lanjut : </span>
			</strong>
		</span>
	</div>
	<div class="w600 cbutton">
		<a href="http://formasi.com/index.php/fortax/demo">
			<div>
				FORTAX video demo
			</div>
		</a>
	</div>
	<br />
	

	<div class="w600 cbutton">
		<a href="http://formasi.com/index.php/fortax/order">
			<div>
				Pesan Sekarang
			</div>
		</a>
	</div>

</div>