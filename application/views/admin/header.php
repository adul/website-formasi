<!DOCTYPE html>
<html >
<head>
	<?php
		$rand = STYLE_VERS;
	?>
	<meta charset="utf-8">
	<title>PNC ADMINISTRATOR</title>
	
	<link href="<?= CSS_PATH .'admin.css?' . $rand ?>" rel="stylesheet" type="text/css" />

	<?
    if (isset($tpl['css']) && count($tpl['css'] > 0 ) ) {
        foreach ($tpl['css'] as  $value) { ?>
            <link rel="stylesheet" href="<?= CSS_PATH . $value . '?' .  $rand ?>" type="text/css" media="screen" /> 
            <?php
        }
    } ?>

	
</head>
<body>
	<div id="header" >
		<div class="welcome-text" >
			Welcome, <span ><?= $tpl['admin']->full_name ?></span>
		</div>
		<div class="box-menu">
			<div class="main-menu">
				<div class="menu-item <?= (@$tpl['menu']['header'] == 'master') ? 'selected active': NULL ?>" id="master">
					MASTER
				</div>
			
				<div class="menu-item <?= (@$tpl['menu']['header'] == 'materials') ? 'selected active': NULL ?>" id="materials">
					MATERIAL
				</div>

				<div class="menu-item <?= (@$tpl['menu']['header'] == 'display') ? 'selected active': NULL ?>" id="display">
					DISPLAY
				</div>
				<div class="menu-item <?= (@$tpl['menu']['header'] == 'content') ? 'selected active': NULL ?>" id="content">
					CONTENT
				</div>
				
				<div class="menu-item <?= (@$tpl['menu']['header'] == 'members') ? 'selected active': NULL ?>" id="members">
					MEMBERS
				</div>

				<div class="menu-item <?= (@$tpl['menu']['header'] == 'setting') ? 'selected active': NULL ?>" id="setting">
					SETTING
				</div>

				<a href="<?= INDEX_URL . 'admin/logout' ?>" >
					<div class="menu-item " id="logout">
						LOGOUT
					</div>
				</a>
			</div>
		</div>
	</div>
			
	<div id="sub-header">
		<div id="cur-selected">
			<?= @$tpl['menu']['child'] ?>
		</div>
		<div class="sub-menu" id="sub-master" style="<?= (@$tpl['menu']['header'] == 'master') ? 'display: inline-block' : NULL ?>">
			<a href="<?= INDEX_URL . 'adminBrands' ?>" >BRANDS</a>
			<a href="<?= INDEX_URL . 'adminArticles' ?>" >ARTICLES</a>
			<a href="<?= INDEX_URL . 'adminItems' ?>" >ITEMS</a>
		</div>
		<div class="sub-menu" id="sub-materials" style="<?= (@$tpl['menu']['header'] == 'materials') ? 'display: inline-block' : NULL ?>">
			<a href="<?= INDEX_URL . 'adminMaterials' ?>" >LIST</a>
			<a href="<?= INDEX_URL . 'adminMaterials/upload' ?>" >UPLOAD</a>
		</div>
		<div class="sub-menu" id="sub-display" style="<?= (@$tpl['menu']['header'] == 'display') ? 'display: inline-block' : NULL ?>">
			<a href="<?= INDEX_URL . 'adminMaterials/display/suggestion' ?>" >SUGGESTION</a>
			<a href="<?= INDEX_URL . 'adminMaterials/display/gallery' ?>" >GALLERY</a>
			<a href="<?= INDEX_URL . 'adminMaterials/display/mixmatch' ?>" >MIX & MATCH</a>
		</div>
		<div class="sub-menu" id="sub-members" style="<?= (@$tpl['menu']['header'] == 'members') ? 'display: inline-block' : NULL ?>">
			<a href="<?= INDEX_URL . 'adminMembers/' ?>" >LIST</a>
			<a href="<?= INDEX_URL . 'adminMembers/sales' ?>" >SALES</a>
		</div>

		<div class="sub-menu" id="sub-content" style="<?= (@$tpl['menu']['header'] == 'content') ? 'display: inline-block' : NULL ?>">
			<?
			foreach (content_section_arr() as $key => $value) { ?>
				<a href="<?= INDEX_URL . 'adminContents/' . $key ?>" ><?= $value ?></a>
		  		<?
		  	} ?>
			<a href="<?= INDEX_URL . 'adminContents/slide_images' ?>" >SLIDE IMAGES</a>

		</div>
		<div class="sub-menu" id="sub-setting" style="<?= (@$tpl['menu']['header'] == 'setting') ? 'display: inline-block' : NULL ?>">
			<a href="<?= INDEX_URL . 'adminSetting/general' ?>" >GENERAL</a>
			<a href="<?= INDEX_URL . 'adminSetting/shippingArea' ?>" >SHIPPING AREA</a>

		</div>
	</div>
	<div id="main-body" >
