// JavaScript Document


$('#sliderButtom').cycle({ 
    fx:     'scrollHorz', 
    speed:  'slow', 
    next:   '#next', 
    prev:   '#prev', 
	timeout: 5000
});

function showUpdateDetail()
{
	var bln = $('#sel-bln').val();
	var thn = $('#sel-thn').val();
	var url = 'show_detail/' + bln + '/' + thn;
	  	$.post(url,{}).done(function(data) {
			$('#detail_content_box').html(data);
			
			$(".BtnDetail").click(function(){
				var id = $(this).attr('rel');
				$("#row_detail_" + id ).slideToggle("slow");
			});
			
			$("#balon").click(function(){
				$("#row2").slideToggle("slow");
			});
			
		});
}

$(document).ready(function(){
  showUpdateDetail();
  $('#update_btn_select').click(function() {
		showUpdateDetail();
	});
  
});


